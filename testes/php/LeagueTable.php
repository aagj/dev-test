<?php
/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

A classe LeagueTablr acompanha o score de cada jogador em uma liga. Depois de cada jogo, o score do jogador é salvo utilizanod a função recordResult.

O Rank de jogar na liga é calculado utilizando a seguinte lógica:

1- O jogador com a pontuação mais alta fica em primeiro lugar. O jogador com a pontuação mais baixa fica em último.
2- Se dois jogadores estiverem empatados, o jogador que jogou menos jogos é melhor posicionado.
3- Se dois jogadores estiverem empatados na pontuação e no número de jogos disputados, então o jogador que foi o primeiro na lista de jogadores é classificado mais alto.


Implemente a funação playerRank que retorna o jogador de uma posição escolhida do ranking.

Exemplo:

$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);


Todos os jogadores têm a mesma pontuação. No entanto, Arnold e Chris jogaram menos jogos do que Mike, e como Chris está acima de Arnold na lista de jogadores, ele está em primeiro lugar.

Portanto, o código acima deve exibir "Chris".


*/

class LeagueTable
{
	public function __construct($players)
    {
		$this->standings = array();
		foreach($players as $index => $p)
        {
			$this->standings[$p] = array
            (
                'index' => $index,
                'games_played' => 0, 
                'score' => 0
            );
        }
	}
		
	public function recordResult($player, $score)
    {
		$this->standings[$player]['games_played']++;
		$this->standings[$player]['score'] += $score;
	}
	
	public function playerRank($rank)
    {
        //ORDENACAO PELA PONTUACAO
        uasort($this->standings, function ($item1, $item2) {
            if ($item1['score'] == $item2['score']){

                //ORDENACAO PELO NUMERO DE JOGOS
                uasort($this->standings, function ($item1, $item2) {
                    if ($item1['games_played'] == $item2['games_played']){
                        return 0;
                    }
                    else {
                        return $item1['games_played'] < $item2['games_played'] ? -1 : 1;
                    }
                });

                return $item1['score'] > $item2['score'] ? -1 : 1;
            }
            else {
                return $item1['score'] > $item2['score'] ? -1 : 1;
            }
        });

        /*ESTÁ ORDENANDO O ARRAY PORÉM NÂO CONSEGUI RETORNAR O VALOR DE FORMA QUE CONSIGA SER PRINTADO NA CHAMADA DA FUNCTION
        TESTEI COM ECHO, VAR_DUMP, PRINT_R, MAS NAO CONSIGO PUXAR A PRIMEIRA POSIÇÃO DO ARRAY PORQUE A CHAVE ASSOCIATIVA ESTÁ PELO NOME E 
        NÂO POR INDICE. NÃO SEI COMO FAZER. TENTEI TRANSFORMAR EM JSON TAMBÉM MAS TAMBÉM NÂO CONSIGO CHAMAR O 
        RESULTADO PASSANDO UM NUMERO COMO PARAMETRO NA CHAMADA
        */

        //echo '<pre>'.print_r($this->standings, true).'</pre>';

        return $this->standings[$rank-1];
	}
}
      
$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 3);
$table->recordResult('Mike', 2);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);


//echo '<pre>'.print_r($table, true).'</pre>';

var_dump($table->playerRank(1));