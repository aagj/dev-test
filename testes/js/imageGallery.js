// Uma galeria de imagens é um conjunto de imagens com botões de remoção correspondentes.
// Este é o código HTML de uma galeria com duas imagens:

// <div class="image">
//   <img src="https://goo.gl/kjzfbE" alt="First">
//   <button class="remove">X</button>
// </div>
// <div class="image">
//   <img src="https://goo.gl/d2JncW" alt="Second">
//   <button class="remove">X</button>
// </div>


// Implemente uma função de configuração que ao receber um evento de click implementa a seguinte lógica:
// * Quando o botão da classe "remove" é clicado, seu elemento div pai deve ser removido da galeria


// Por exemplo, depois que a primeira imagem da galeria acima foi removida, o código HTML ficaria assim:

// <div class="image">
//   <img src="https://goo.gl/d2JncW" alt="Second">
//   <button class="remove">X</button>
// </div>



function setup () {
  console.log("teste")

  //Por algum motivo está excluindo apenas depois do segundo click no X

  $(".remove").click(function(){ 

    //.closest procura o elemento mais próximo que satisfaça o critério ("div"), começando a partir de si mesmo e subindo para os nós pais.
    $(this).closest('div').remove();
    return false;
  });

}

//TESTADO NO imageGallery.html pois as imagens estavam quebradas

