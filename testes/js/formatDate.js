// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.


function formatDate(userDate) {
  // format from M/D/YYYY to YYYYMMDD

  //Parte a string com a data em variáveis de dia, mes, ano
  var dia  = userDate.split("/")[0];
  var mes  = userDate.split("/")[1];
  var ano  = userDate.split("/")[2];

  //Concatena os valores partidos na ordem e formato que desejar,
  //"0" acrescenta um zero no formato, assim tanto faz passar a data como "9/6/2021" como "09/06/2021", sempre vai ser dois digitos, o método Slice(-2) faz utilizar somente os dois ultimos digitos, caso contrário se a data fosse passada como "09/06/2021" e concatenasse mais um zero, iria ficar formatado como "2021/006/009"
  
  return ano + '/' + ("0"+mes).slice(-2) + '/' + ("0"+dia).slice(-2);
}

console.log(formatDate("09/06/2021"));