// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.



//OBJETO COM PROPRIEDADES PARA TESTE
var objeto = {
  prop1: "valor1",
  prop2: "valor2",
  prop3: "valor3",
}


function removeProperty(objeto, prop) {
  //VERIFICA SE PROPRIEDADE FOI PASSADA
  if(prop != undefined) {

    //VERIFICA SE A PROPRIEDADE PASSADA EXISTE NO OBJETO
    if(objeto.hasOwnProperty(prop)) {
      //DELETA PROPRIEDADE PASSADA
      return delete objeto[prop];
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }

}

//PASSA OBJETO E PROPRIEDADE A SER DELETADA PARA A FUNCTION
console.log(removeProperty(objeto, "prop2"));
//PRINTA OBJETO APÓS DELEÇÃO
console.log(objeto);
